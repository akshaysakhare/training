<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="../login/index.php">Isecure</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page"  href="../login/welcome.php">Home</a>
        </li>
        <?php
        error_reporting(0);
        session_start();
        if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true){
          ?>
        <li class="nav-item">
          <a class="nav-link"  href="../login/login.php">login</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link "  href="../login/signup.php">signup</a>
        </li>
            <?php }else{
              ?>
           
        <li class="nav-item">
          <a href="../login/logout.php"class="nav-link ">Logout</a>
        </li>
        <?php } ?>
      </ul>
      <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
</nav>