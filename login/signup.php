<?php 
$showalert=false;
$showerror=false;
//error_reporting(0);
if($_SERVER["REQUEST_METHOD"] == "POST"){
    require 'dbconn.php';
    
    $username =$_POST['username'];
    $password =$_POST['password'];
    $cpassword =$_POST['cpassword'];
    //$exists == false;
// checking the already exixt user
    $existsql="SELECT * FROM users  WHERE username='$username'";
    $results=mysqli_query($conn,$existsql);
    $numexistrow=mysqli_num_rows($results);
            if($numexistrow > 0){
               // $exists=true;
                $showerror="user already exists";

            }else{
              //  $exists == false;
           
                    if(($password==$cpassword)){
                        $hash = password_hash($password, PASSWORD_DEFAULT);
                            // hash function to encrypt the password
                        $sql ="INSERT INTO `users` (`username`, `password`, `dt`) VALUES ('$username', '$hash', current_timestamp())";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            $showalert=true;
                        }else{
                            
                            }
                    }else{
                        $showerror= "password and confirm password not match";
                    }

            }
    }





?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Signup</title>
  </head>
  <body>
      <?php require 'partials/nav.php'; ?>


            <?php
                if($showalert){
           echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong> Your account has been created.
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>';
                }

                if($showerror){
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                     <strong>Success!</strong>'. $showerror.'
                     <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                     </div>';
                         }
            ?>


    <div class="container my-4">
        <h1 class="text-center">Signup to our website</h1>

        <form action="" method="post">
                <div class="col-md-6">
                    <label for="exampleInputEmail1" class="form-label">Username</label>
                    <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    
                </div>
                <div class="col-md-6">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input type="password"  name="password" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="col-md-6">
                    <label for="exampleInputPassword1" class="form-label">confirm Password</label>
                    <input type="password" name="cpassword" class="form-control" id="exampleInputPassword1">
                    <div id="emailHelp" class="form-text">Type the same password enter in password fiels.</div>
                </div>

                <!-- <div class="mb-3 form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div> -->
                
                <button type="submit" class="btn btn-primary col-md-6">Signup</button>
                
        </form>

    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>