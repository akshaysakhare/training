<?php 
$fptr = fopen("file.txt","r");
// fgets() function reads the file line by line

// echo fgets($fptr); //prints first line only
// echo fgets($fptr);//prints second line only
// echo fgets($fptr);//prints third line only
// echo var_dump(fgets($fptr)); // not exist the line hence return the fasle value



// print all lines of file using loopby line by line
/*
while($a=fgets($fptr)){
    echo $a;
}

*/
//echo "last";

echo "fgetc function reads the file character by character<br>";
echo fgetc($fptr);  // reads the first character of file
//program
while($a=fgetc($fptr)){
    echo $a;
    if($a=="."){ //print the file till . found
        break;
    }
}
fclose($fptr);

?>