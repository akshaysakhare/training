<?php
class Employee{
    public $name;
    //public $lang;
    Public $salary;

    public function __construct($name,$salary){
        $this->name=$name;
        //$this->lang=$lang;
        $this->salary=$salary;
        $this->describe();
    }

    protected function describe(){
        echo "name of programmer $this->name <br>";
       // echo "Lang of programmer $this->lang<br>";
        echo "Salary of programmer $this->salary <br>";
    }
}
class programmer extends Employee{
    public $lang= "php";
    public function __construct($name, $lang, $salary){
        $this->name=$name;
        $this->lang=$lang;
        $this->salary=$salary;
        $this->describe();
    }
}
$akshay = new Employee("akshay", 5000);
$rohan = new programmer("rohan","python", 5000);
//$akshay->describe();

?>