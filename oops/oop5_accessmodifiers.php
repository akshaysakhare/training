<?php
echo "access modifiers in php<br>";
//public can be accessed by anywhere
//private can only be accessed from within the class
//pritected - can be accessed from within the class or drived class
echo"1.public.<br> 2Private <br>3 Protected<br>";
// by default the properties and methods are treated as public
//private properties and methods can only be accessed by other member
//function of class

class Employee{
    private $name ="akshay";
    
    public function showname(){
        echo "$this->name";
    }
}

$akshay=new Employee();
// echo $akshay->name; //this will not work if akshay is private
$akshay->showname();

?>