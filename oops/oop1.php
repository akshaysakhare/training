<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>introduction to OOPs concept in PHP</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>
    <div class="container my-4 ml-5">
    <h1>This is the Oops Tutorial 1</h1>
    <h2>What is Oops?</h2>
        <p>OOPs is about creating classes and objects. Class serves as a template and multiple objects can be created using a class</p>

        <h2>What are Classes and Objects</h2>
                <p>Classes are templates for creating objects</p>
                <p>If car is a class then maruti suzuki alto and maruti Swift are two objects</p>
    
    <h2>Why OOPs</h2>
    <p>OOPs makes it easy to keep the code DRY? </p>
    <p><b>DRY </b>- Do not repeat yourself - the code written by the programmer should be reusable</p> 
    </div>
</body>
</html>