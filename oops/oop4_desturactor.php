<?php

    class Employee{
        //properties of a class
        public $name;
        public $salary;        
        function __construct($name,$salary){
            $this->name=$name;
            $this->salary=$salary;
        }
        //destructor
        function __destruct(){
            echo "i am destructing $this->name <br>";
        }
    }

    $akshay= new Employee("akshay",1000);
    $harry= new Employee("harry",20000);
    $skillf= new Employee("skillf",10000);

    echo "The salary of akshay is $akshay->salary <br>";
    echo "The salary of akshay is $harry->salary <br>";
?>