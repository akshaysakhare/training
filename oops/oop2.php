<?php
//echo "Oops -2 in PHP<br>";

class Player {
    //properties of the class
    public $name;
    public $speed = 3;
    public $running=false;


    // methods of class
    function set_name($name) {
        $this->name = $name;
    }
    function get_name() {
        return $this -> name;
    }
    function run(){
        $this->running=true;
    }
    function stoprun(){
        $this->running=true;
    }

}
$player1 = new Player();
$player1 -> set_name("akshay");// setting name
echo $player1->get_name();// getting name
echo "<br>";
echo $player1->speed;
echo "<br>";

$player2 = new Player();
$player2 -> set_name("Harry");// setting name
echo "<br>";
echo $player2->get_name();// getting name
echo "<br>";
?>