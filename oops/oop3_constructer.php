<?php

    class Employee{
        //properties of a class
        public $name;
        public $salary;


         // Methods of Our Class
        // Constructor - It allows you to initialize objects. It is the code which is executed whenever a new object is instantiated.

        // Constructor without arguments
        // function __construct(){
        //     echo "This is my constructor for employee";
        // function which run on creation of object.
        // construct without arguments
        // function __construct(){
        //     echo "this is constructor for employee";
        // }
        // construct with arguments
        function __construct($name,$salary){
            $this->name=$name;
            $this->salary=$salary;
        }
    }

    $akshay= new Employee("akshay",1000);
    $harry= new Employee("harry",20000);
    $skillf= new Employee("skillf",10000);

    echo "The salary of akshay is $akshay->salary <br>";
    echo "The salary of akshay is $harry->salary";
?>