<?php
echo "what is inheritance<br>";
class Employee{
    public $name ="Akshay";
    private $salary =12000;
    private $grade =3;

    function setsalary($salary){
        $this->salary=$salary;
    }

    function getsalary(){
        echo "The salary of the employee $this->name is $this->salary<br>";
    }

    function showname(){
        echo "The name of the employee is $this->name<br>";
    }
}
// Inheriting a new class Programmer from Employee

class programmer extends Employee{
    private $lang ="php";
    function changelanguage($lang){
        $this->lang=$lang;
        echo $this->grade; // this will throww thrrow an err because great is private in the  parent/base class
       //echo" $this->lang=$lang <br>";
    }
}

$akshay = new employee();
$akshay->name="akshay";
//$akshay->salary=100;// not working shows the error beecause of salary is a private property
$akshay->setsalary(10000);
$akshay->getsalary();
$akshay->showname();

$shubham = new employee();
$shubham->name="shubham";
//$shubham->salary=20100;// not working shows the error beecause of salary is a private property
$shubham->setsalary(20000);
$shubham->getsalary();
$shubham->showname();


$geeta = new programmer();
$geeta->name="Geeta";
echo $geeta->changelanguage("python");
$geeta->getsalary();
$geeta->showname();
?>