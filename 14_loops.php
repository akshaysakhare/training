<?php 
// for loop
for($i=1;$i<=10;$i++){
    echo "value of i is ".$i. "<br>";
}

// do while loop
$i=15;
do{
    echo "The value of i ". $i."<br>";// print this statement even the condition not true
    $i++;
}while($i<10);

?>