<?php 

$name = "Akshay";
$income=200;
//php data type

/*
1. String 
2. Integer
3. float
4. Boolean
5. Object
6. Array
7. NULL
*/


// sttring sequence of characters
$name="akshay";
$friend="harry";
echo "I am $name and my friend is $friend";
echo "<br>";


// Integer - non decimal number
 $income=200;
 $debts=-100;

 echo "my income is $income and debets is $debts";
 echo "<br>";


 // float decimal number

$income=200.50;
 $debts=-100.20;

 echo "my income is $income and debets is $debts";
 echo "<br>";
 echo "total = ".($income+$debts);

 echo "<br>";

 // can be Boolean Eighter true of false

 $x=true;
 $is_friend=false;
 echo var_dump($x);
 echo "<br>";
 echo var_dump($is_friend);
 echo "<br>";



 // Object - Instances of class


 //Array- Use to store multipal values in a single variables
 $friends = array("akshay","arvind", "dnyanu");
 echo var_dump($friends);
 echo "<br>";
 echo $friends[0];
 echo "<br>";
 echo $friends[1];
 echo "<br>";
 echo $friends[2];
 echo "<br>";
 echo $friends[3]; // thriws error as this array is 3
 echo "<br>";
 // array index is always start with 0



 // NULL
 $name=NULL;
 echo var_dump($name);


?>