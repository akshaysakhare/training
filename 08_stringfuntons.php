<?php 

$name="Akshay is good Boy";
echo $name;
echo "<br>";

echo "The length of my string is " . strlen($name);//count the characters of string
echo "<br>";

echo str_word_count($name);// count words of string
echo "<br>";

echo strrev($name);// reverse the string
echo "<br>";

echo strpos($name, "is");// display the index of the is in the string which is start with 0
echo "<br>";

echo str_replace("Akshay","Arvind",$name);//replace function which exchange the word 'Akshay' By 'Arcind' in the name string
echo "<br>";


echo str_repeat($name,5);
echo "<br>";


echo "<pre>";
echo rtrim("   this is a good boy    ");// cut the spaces of string from left 

echo "<br>";
echo ltrim("   this is a good boy    ");// cut the spaces of string from right 
echo "</pre>";
echo "<br>";
?>